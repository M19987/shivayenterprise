//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyApplication.Models
{
    using System;
    
    public partial class sp_g_PriceInfo_Result
    {
        public Nullable<decimal> Price { get; set; }
        public string Quantity { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
    }
}
