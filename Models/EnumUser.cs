﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyApplication.Models
{
    public class EnumUser
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public decimal Mobileno { get; set; }
        public string Password { get; set; }
        public int UserTypeId { get; set; }

        [Display(Name = "Gender")]
        public Genders Gender { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
    public enum Genders
    {
        Male = 1,
        Female = 2
    }


}