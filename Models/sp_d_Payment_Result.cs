﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace MyApplication.Models
{
    public class sp_d_Payment_Result
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string SenderName { get; set; }
        public int OrderId { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}