﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApplication.Models.Repository
{
    public interface I_BasicEntity<T> where T : class
    {
        IEnumerable<T> GetModel();
        T GetModelById(int id);
        void Insert(T entity);
        void Update(T entity);
        void save();
        void Delete(int id);
    }
}
