﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyApplication.Models.Repository
{
    public class BasicEntity<T> : I_BasicEntity<T> where T : class
    {
        private ShivayEnterpriseEntities _context;
        private IDbSet<T> dbEntity;

        public BasicEntity()
        {
            _context = new ShivayEnterpriseEntities();
            dbEntity = _context.Set<T>();
        }

        public void save()
        {
            _context.SaveChanges();
        }

        public IEnumerable<T> GetModel()
        {
            return dbEntity.AsEnumerable();
        }

        public T GetModelById(int modelId)
        {
            return dbEntity.Find(modelId);
        }

        public void Insert(T model)
        {
            dbEntity.Add(model);
        }

        public void Update(T model)
        {
            _context.Entry(model).State = System.Data.Entity.EntityState.Modified;
        }

        public void Delete(int modelId)
        {
            T model = dbEntity.Find(modelId);
            dbEntity.Remove(model);
        }
    }
}