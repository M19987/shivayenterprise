﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyApplication.Models;
using PagedList;

namespace MyApplication.Controllers
{
    public class ProductsController : Controller
    {
        private ShivayEnterpriseEntities db = new ShivayEnterpriseEntities();

        // GET: Products
        public ActionResult Index(int? page)
        {
            var pageNumber = page ?? 1;
          
            var products = db.Products.ToList();
            return View(products.ToPagedList(pageNumber, 4));
        }

        [HttpPost]
        public ActionResult Index(string ProductsName,int? page)
        {
            List<Product> pt;
            if (string.IsNullOrEmpty(ProductsName))
            {
                pt = db.Products.ToList();
            }
            else
            {
                pt = db.Products.Where(x => x.ProductName.StartsWith(ProductsName)).ToList();
            }
            return View(pt.ToPagedList(page ?? 1,4));
        }
        [HttpPost]
        public JsonResult AutoComplete(string prefix, int? page)
        {
            var Product = (from x in db.Products
                           where x.ProductName.Contains(prefix)
                           select new
                           {
                               label = x.ProductName,
                               val = x.Id
                           }).ToList();

            return Json(Product.ToPagedList(page ?? 1,4));
        }
        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.SeedsId = new SelectList(db.PlasticSeeds, "Id", "SeedsName");
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Id");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductName,Size,Color,Price,SeedsId,SupplierId,CreatedDate,ModifiedDate")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SeedsId = new SelectList(db.PlasticSeeds, "Id", "SeedsName", product.SeedsId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Id", product.SupplierId);
            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.SeedsId = new SelectList(db.PlasticSeeds, "Id", "SeedsName", product.SeedsId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Id", product.SupplierId);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProductName,Size,Color,Price,SeedsId,SupplierId,CreatedDate,ModifiedDate")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SeedsId = new SelectList(db.PlasticSeeds, "Id", "SeedsName", product.SeedsId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Id", product.SupplierId);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
