﻿using MyApplication.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace MyApplication.Controllers
{
    public class PaymentsController : Controller
    {
        private ShivayEnterpriseEntities db = new ShivayEnterpriseEntities();
        // GET: Payments
        public ActionResult Index()
        {
            return View(db.sp_s_Payment().ToList());
        }

        // GET: Payments/Details/5
        public ActionResult Details(int id)
        {
            var payment = db.sp_g_Payment(id).FirstOrDefault();
            return View(payment);
        }

        // GET: Payments/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName");
            return View();
        }

        // POST: Payments/Create
        [HttpPost]
        public ActionResult Create(Payment payment)
        {
            try
            {
                var PayTransaction = db.sp_create_Payment(payment.Id, payment.UserId, payment.SenderName, payment.OrderId, Convert.ToInt32(payment.Price));
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return View(payment);
        }

        // GET: Payments/Edit/5
        public ActionResult Edit(int id)
        {
            var Paymentview = db.sp_g_Payment(id).First();
            return View(Paymentview);
        }

        // POST: Payments/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Payment payment)
        {
            try
            {
                    var paymentEdit = db.sp_u_Payment(payment.Id, payment.UserId, payment.SenderName, payment.OrderId, payment.Price, payment.PaymentDate);
                    return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return View(payment);
        }

        // GET: Payments/Delete/5
        public ActionResult Delete(int id)
        {
            var Paymentview = db.sp_g_Payment(id).First();
            return View(Paymentview);
        }

        // POST: Payments/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Payment collection)
        {
            try
            {
                db.sp_d_Payment(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(ex);
            }

        }
    }
}
