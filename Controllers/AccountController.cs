﻿using MyApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MyApplication.Controllers
{
    public class AccountController : Controller
    {
        ShivayEnterpriseEntities db = new ShivayEnterpriseEntities();
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        // GET: Account/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Account/Create
        public ActionResult Register()
        {
            var users1 = db.UserTypes.ToList();
            List<SelectListItem> UserType = new List<SelectListItem>();
            foreach (var item in users1)
            {
                UserType.Add((new SelectListItem()
                {
                    Text = item.UserType1,
                    Value = item.Id.ToString(),
                }));
            }
            ViewData["UserType"] = UserType;
            return View();
        }

        // POST: Account/Create
        [HttpPost]
        public ActionResult Register(User users)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var Mobileno = db.Users.FirstOrDefault(x => x.Mobileno == users.Mobileno && x.EmailId == users.EmailId);
                    if (Mobileno == null)
                    {
                        db.Users.Add(users);
                        db.SaveChanges();
                        var activationCode = Guid.NewGuid();
                        SendEmail(users.EmailId, activationCode.ToString());
                        //ViewBag.Message = "Registration Completed Succesfully!!!, Please Check Your Mail : " + users.EmailId;
                        TempData["SuccessMessage"] = "Registration Completed Succesfully!!!, Please Check Your Mail : " + users.EmailId;
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Already Registered!!!!";
                        return RedirectToAction("Register");
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return View();
        }
        public bool SendEmail(string emailId, string activationcode)
        {
            try
            {
                var userverificationlink = "/Account/Login/" + activationcode;
                var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, userverificationlink);

                var fromMail = ConfigurationManager.AppSettings["smtpUser"].ToString();
                var fromEmailpassword = ConfigurationManager.AppSettings["smtpPass"].ToString();
                var toEmail = new MailAddress(emailId);
                var smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(fromMail, fromEmailpassword);
                MailMessage mailMessage = new MailMessage(fromMail, emailId);
                mailMessage.Subject = "Login Completed-Demo";
                mailMessage.Body = "<br/> Your registration completed succesfully." +
                               "<br/> please click on the below link for account verification" +
                                "<br/><br/><a href=" + link + ">" + link + "</a>" +
                                "<br/>OTP For Verification:" ;
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                smtp.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string GeneratePassword()
        {
            string OTPLength = "4";
            string OTP = string.Empty;

            string Chars = string.Empty;
            Chars = "1,2,3,4,5,6,7,8,9,0";

            char[] seplitChar = { ',' };
            string[] arr = Chars.Split(seplitChar);
            string NewOTP = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < Convert.ToInt32(OTPLength); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                NewOTP += temp;
                OTP = NewOTP;
            }
            return OTP;
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User users)
        {
            var data = db.Users.FirstOrDefault(x => x.EmailId == users.EmailId && x.Password == users.Password);
            if (data != null)
            {
                Session["Id"] = data.Id;
                Session["EmailId"] = data.EmailId;
                Session["Mobileno"] = data.Mobileno;
                Session["Password"] = data.Password;
                Session["UserTypeId"] = data.UserTypeId;
                return RedirectToAction("Index", "User");
            }
            else
            {
                ModelState.AddModelError("", "Invalid Information... Please try again!");
                return RedirectToAction("Login");
            }
            return View();
        }

     



        // GET: Account/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Account/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Account/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
