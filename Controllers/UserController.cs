﻿using MyApplication.Models;
using MyApplication.Models.Repository;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyApplication.Controllers
{
    public class UserController : Controller
    {
        ShivayEnterpriseEntities db = new ShivayEnterpriseEntities();
        private I_BasicEntity<User> interfaceobj;
        public UserController()
        {
            this.interfaceobj = new BasicEntity<User>();
        }
        public ActionResult Index(String Searchbox, String CurrentFilter, String UserType, String Id, int? page)
        {
            List<UserType> UserType1 = db.UserTypes.ToList();
            ViewBag.UserType = new SelectList(UserType1, "Id", "UserType1");
            ViewBag.Users = new SelectList("", "Id", "FirstName");
            var tempmessage = TempData["SuccessMessage"];
            if (!string.IsNullOrEmpty(UserType))
            {
                List<User> UserList = db.Users.Where(x => x.UserTypeId.ToString() == UserType).ToList();
                ViewBag.Users = new SelectList(UserList, "Id", "FirstName");
            }

            //searchbar
            CurrentFilter = Searchbox;

            var pageNumber = page ?? 1;
            int userIdInt;
            var userIdInt1 = Int32.TryParse(Id, out userIdInt);
            int userTypeInt;
            var userTypeInt1 = Int32.TryParse(UserType, out userTypeInt);
            var userTypeObj = new List<User>();
            var userObj = new List<User>();

            if (!String.IsNullOrEmpty(UserType) && !String.IsNullOrEmpty(Id))
                userObj = interfaceobj.GetModel().Where(x => x.Id == Convert.ToInt32(Id)).ToList();


            else if (!String.IsNullOrEmpty(UserType))
                userTypeObj = interfaceobj.GetModel().Where(x => x.UserTypeId == userTypeInt).ToList();

            var searchObj = new List<User>();
            if (!String.IsNullOrEmpty(Searchbox))
                searchObj = interfaceobj.GetModel().Where(x => x.FirstName.Contains(Searchbox)).ToList();

            var mergeObj = String.IsNullOrEmpty(UserType) && String.IsNullOrEmpty(Searchbox) && String.IsNullOrEmpty(UserType) ? interfaceobj.GetModel().ToList() : userObj.Concat(searchObj).Concat(userTypeObj);
            return View(mergeObj.ToList().ToPagedList(pageNumber, 4));
        }

        public JsonResult GetCascadingList(int UserTypeId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<User> UserList = db.Users.Where(x => x.UserTypeId == UserTypeId).ToList();
            return Json(UserList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            var udetails = interfaceobj.GetModelById(id);
            return View(udetails);
        }


        public ActionResult Edit(int id)
        {
            User user = interfaceobj.GetModelById(id);
            return View(user);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, User collection)
        {
            try
            {
                interfaceobj.Update(collection);
                interfaceobj.save();
                return RedirectToAction("Index");
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(collection);
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            User u = interfaceobj.GetModelById(id);
            return View(u);
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, User collection)
        {
            try
            {
                // TODO: Add delete logic here
                interfaceobj.Delete(id);
                interfaceobj.save();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View();
        }
    }
}
