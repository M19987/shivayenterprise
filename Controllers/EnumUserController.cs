﻿using MyApplication.Models;
using MyApplication.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyApplication.Controllers
{
    public class EnumUserController : Controller
    {
        ShivayEnterpriseEntities db = new ShivayEnterpriseEntities();
       // private I_BasicEntity<User> interfaceobj;
        public EnumUserController()
        {
           // this.interfaceobj = new BasicEntity<EnumUser>();
        }
        // GET: EnumUser
       
        public ActionResult Index(String Gender)
        {
            var userObj = new List<EnumUser>();

           // if (!String.IsNullOrEmpty(Gender))
              //  userObj.ToList();

            //if (Gender == (1).ToString())
            //    userObj.Where(x => x.Gender == Genders.Male);

            //if (Gender == (2).ToString())
            //    userObj.Where(x => x.Gender == Genders.Female);

            return View();
        }

        // GET: EnumUser/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EnumUser/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EnumUser/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EnumUser/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EnumUser/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EnumUser/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EnumUser/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
