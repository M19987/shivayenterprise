﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyApplication.Extension_Methods
{
    public static class StringExtensions
    {
        public static string Count(this string input)
        {
            return input.Count();
        }

    }
}